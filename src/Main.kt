fun main() {

    val s = sayHello("Jemali")
    println(s)
    sayHello("Nargizi")

}

fun sayHello(name: String): String {
    return "Hello, $name!"
}